(function() {
    // inputs
    let cityInput = document.getElementById("city");
    let phoneInput = document.getElementById("phone");

    // buttons
    let saveButton = document.querySelector(".save-btn");
    let resetButton = document.querySelector(".reset-btn");

    // regexps
    const phoneValidation = /^[7-8][9]\d{9}$/i; // формат российских номеров 79XXYYYDDHH || ^(\+7|7|8)?[489]\d{9}$
    const altPhoneValidation = /^[+]*[(]?[0-9]{1,3}[)]?[-\s\./0-9]*$/g; // куча форматов сразу

    const configRequest = {
        MIN_VALUES: 3,
        URLS: [
            "./src/assets/data.json",
            "./src/assets/data2.json",
            "./src/assets/data3.json"
        ]
    };


    class OwnInput {
        constructor(config) {
            if (!config.el) {
                throw new Error(`"el" option required`);
            }

            this._el = config.el;

            this.init();
        }

        init() {
            this.inputHandler = this.inputHandler.bind(this);

            this._el.addEventListener("input", this.inputHandler);

            let initFoundNodesElement = document.createElement("div");
            initFoundNodesElement.classList.add("found-list");

            this.foundNodes = new FoundList(initFoundNodesElement, this._el);
        }

        inputHandler({ target }) {
            const { value } = target;

            if (!this.foundNodes.isDestroyed) {
                this.foundNodes.destroy();
            }

            if (value !== "") {
                let requestData = this.getFilteredData(value);
                requestData.then(actualData => {
                    if (actualData.length > 0) {
                        this.foundNodes.appendListItems(actualData);
                        this.foundNodes.append(this._el);
                    }
                });
            }
        }

        destroyFoundList() {
            this.foundNodes.destroy();
        }

        filter(arrayToBeFiltered, regexpValue) {
            let re = new RegExp(regexpValue, "gi");
            // иногда возникают траблы с методом .test в фильтре, поэтому можно заменить на v.search(re) !== -1
            // будет работать вроде как правильно, нежели re.test(value)
            return arrayToBeFiltered.filter(value => value.search(re) !== -1);
        }

        async getFilteredData(inputValue) {
            let urlsToFetch = [...configRequest.URLS];
            let outputFilteredData = [];

            for (let url of urlsToFetch) {
                if (outputFilteredData.length >= configRequest.MIN_VALUES) {
                    break;
                }
                let response = await fetch(url);
                let result = await response.json();

                let currentFilteredDataFromUrl = this.filter(result.data, inputValue);
                outputFilteredData = outputFilteredData.concat(currentFilteredDataFromUrl);
            }
            return outputFilteredData;
        }
    }

    class FoundList {
        constructor(el, parentEl) {
            if (!el) {
                throw new Error(`"el" argument required`);
            }
            this.el = el;
            this._isDestroyed = false;
            this.parentEl = parentEl;

            this.init();
        }

        get isDestroyed() {
            return this._isDestroyed;
        }

        set isDestroyed(is) {
            this._isDestroyed = is;
        }

        init() {
            let me = this;
            this.el.addEventListener("click", function (e) {
                if (e.target && e.target.classList.contains("found-item")) {
                    me.parentEl.value = e.target.textContent;
                    me.destroy();
                }
            });
        }

        append(node) {
            if (!node) {
                throw new Error(`"node" argument required`);
            }

            node.parentElement.insertAdjacentElement("beforeend", this.el);
            this.isDestroyed = false;
        }

        appendListItems(data) {
            this.clearListItems();

            if (data instanceof Array) {
                for (let value of data) {
                    let listItem = document.createElement("div");
                    listItem.classList.add("found-item");
                    listItem.textContent = value;
                    this.el.appendChild(listItem);
                }
            }
        }

        clearListItems() {
            let listItems = this.el.getElementsByTagName("div");

            if (listItems) {
                for (let i = listItems.length - 1; i >= 0; i--) {
                    this.el.removeChild(listItems[i]);
                }
            }
        }

        destroy() {
            this.el.remove();
            this.isDestroyed = true;
        }
    }

    ({
        init: function(global) {
            let { localStorage, setTimeout, fetch, FormData } = global;
            let city = localStorage.getItem("city"),
                phone = localStorage.getItem("phone");

            if(city && phone) {
                cityInput.value = city;
                phoneInput.value = phone;
            }

            const input = new OwnInput({
                el: cityInput
            });

            // delegate click event on document, cause found list nodes created dynamically
            document.addEventListener("click", function (e) {
                if (!e.target.classList.contains("found-item")) {
                    input.destroyFoundList();
                }
            });

            saveButton.addEventListener("click", function() {
                let cityInputValue = cityInput.value,
                    cityInputStyle = cityInput.style,
                    phoneInputValue = phoneInput.value,
                    phoneInputStyle = phoneInput.style;

                if(!cityInputValue) {
                    cityInputStyle.background = "#eabebe";
                    setTimeout(() => cityInputStyle.background = "white", 2000);
                    return;
                }

                if(!phoneInputValue || phoneInputValue.search(phoneValidation) === -1) {
                    phoneInputStyle.background = "#eabebe";
                    setTimeout(() => phoneInputStyle.background = "white", 2000);
                    return;
                }

                if(cityInputValue && phoneInputValue && phoneInputValue.search(phoneValidation) !== -1) {
                    localStorage.setItem("city", cityInputValue);
                    localStorage.setItem("phone", phoneInputValue);

                    const formData = new FormData();
                    formData.append("city", cityInputValue);
                    formData.append("phone", phoneInputValue);

                    let savedBlock = document.querySelector(".is-saved");
                    savedBlock.classList.toggle("hidden");

                    fetch("/save_form", { method: "POST", body: formData })
                        .then(response => response.json()).then(result => console.log("success"))
                        .catch(error => console.log(`Error: ${error.message}`))
                        .finally(() => {
                            setTimeout(() => {
                                savedBlock.classList.toggle("hidden");
                            }, 2000);
                        });
                }
            });

            resetButton.addEventListener("click", function() {
                let city = localStorage.getItem("city"),
                    phone = localStorage.getItem("phone");

                if(city && phone) {
                    localStorage.removeItem("city");
                    localStorage.removeItem("phone");
                }

                cityInput.value = "";
                phoneInput.value = "";
            });
        }
    }).init(this);
})();